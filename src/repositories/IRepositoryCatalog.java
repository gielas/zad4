package repositories;

public interface IRepositoryCatalog {

	public IEnumerationValueRepository enumerations();
	public IUserRepository users();
}
