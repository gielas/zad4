package repositories;

public class PagingInfo {
	
	private int pageSize;
	private int page;
	private int totalCount;
	
	public PagingInfo () {}
	
	public PagingInfo (int pageSize, int page, int totalCount) {
		this.pageSize = pageSize;
		this.page = page;
		this.totalCount = totalCount;
	}
	
	public int getPageSize() { 
		return pageSize; 
	}
	
	public void setPageSize(int pageSize) { 
		this.pageSize = pageSize; 
	}
	
	public int getPage() { 
		return page; 
	}
	
	public void setPage(int page) { 
		this.page = page; 
	}
	
	public int getTotalCount() { 
		return totalCount; 
	}
	
	public void setTotalCount(int totalCount) { 
		this.totalCount = totalCount; 
	}
}
