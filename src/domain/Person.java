package domain;

import java.util.ArrayList;
import java.util.List;

public class Person extends Entity {
	
	private String firstName;
	private String surname;
	private User user;
	private List<Address> addresses;
	
	public Person() {
		super();
		this.addresses = new ArrayList<Address>();
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
}
