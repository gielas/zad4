package domain;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity {
	
	private String login;
	private String password;
	private List <UserRoles> userRoles;
	private List <RolesPermissions> rolesPermissions;
	
	public User () {
		userRoles = new ArrayList <UserRoles> ();
		rolesPermissions = new ArrayList <RolesPermissions> ();
		setState(EntityState.New);
	}
	public String getLogin() { 
		return login; 
	}
	
	public void setLogin(String login) { 
		this.login = login; 
	}
	
	public String getPassword() { 
		return password; 
	}
	
	public void setPassword(String password) { 
		this.password = password; 
	}
	
	public List<UserRoles> getUserRoles() { 
		return userRoles; 
	}
	
	public void setUserRoles(List<UserRoles> userRoles) { 
		this.userRoles = userRoles; 
	}
	
	public List<RolesPermissions> getUserPermissions() { 
		return rolesPermissions; 
	}
	
	public void setUserPermissions(List<RolesPermissions> userPermissions) { 
		this.rolesPermissions = userPermissions; 
	}
}
