package domain;

public class UserRoles extends Entity {
	
	private int userId;
	private int roleId;
	
	public UserRoles () {
		setState(EntityState.New);
	}
	
	public UserRoles (int userId, int roleId) {
		this();
		this.userId = userId;
		this.roleId = roleId;
	}
	
	public int getUserId() { 
		return userId; 
	}

	public void setUserId(int userId) { 
		this.userId = userId; 
	}
	
	public int getRoleId() { 
		return roleId; 
	}

	public void setRoleId(int roleId) { 
		this.roleId = roleId; 
	}	
}
