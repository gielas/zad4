package domain;

public class RolesPermissions extends Entity {
	
	private int roleId;
	private int permissionId;
	
	public RolesPermissions () {}
	
	public RolesPermissions (int roleId, int permissionId) {
		this.roleId = roleId;
		this.permissionId = permissionId;
	}
	
	public int getRoleId() { 
		return roleId; 
	}
	
	public void setRoleId(int roleId) { 
		this.roleId = roleId; 
	}
	
	public int getPermissionId() { 
		return permissionId; 
	}
	
	public void setPermissionId(int permissionId) { 
		this.permissionId = permissionId; 
	}	
}
